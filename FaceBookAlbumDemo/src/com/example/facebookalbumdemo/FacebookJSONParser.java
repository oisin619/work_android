package com.example.facebookalbumdemo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class FacebookJSONParser
{
	public static String parseLatestStatus(String json)
	{
		String latestStatus="";
		String startTag="\"message\":\"";
		int indexOf=json.indexOf(startTag);
		if(indexOf>0)
		{
			int start=indexOf+startTag.length();
			String endTag="\",";
			return(json.substring(start,json.indexOf(endTag,start)));
		}
		return(latestStatus);
	}
	public static ArrayList<AlbumItem> parseAlbums(String json) throws JSONException
	{
		ArrayList<AlbumItem> albums=new ArrayList<AlbumItem>();
		JSONObject rootObj=new JSONObject(json);
		JSONArray itemList=rootObj.getJSONArray("data");
		int albumCount=itemList.length();
		for(int albumIndex=0;albumIndex<albumCount;albumIndex++)
		{
			JSONObject album=itemList.getJSONObject(albumIndex);
			String description="";
			try{
				description=album.getString("description");
			}catch(JSONException x)
			{/*not implemented*/}
			albums.add(new AlbumItem(album.getString("id"),album.getString("name"),description));
		}
		return(albums);
	}
	public static ArrayList<PhotoItem> parsePhotos(String json) throws JSONException
	{
		ArrayList<PhotoItem> photos=new ArrayList<PhotoItem>();
		JSONObject rootObj=new JSONObject(json);
		JSONArray itemList=rootObj.getJSONArray("data");
		int photoCount=itemList.length();
		for(int photoIndex=0;photoIndex<photoCount;photoIndex++)
		{
			JSONObject photo=itemList.getJSONObject(photoIndex);
			photos.add(new PhotoItem(photo.getString("picture"),photo.getString("source")));
		}
		return(photos);
	}
}