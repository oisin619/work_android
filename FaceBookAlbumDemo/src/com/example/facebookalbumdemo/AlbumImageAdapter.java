package com.example.facebookalbumdemo;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class AlbumImageAdapter extends BaseAdapter
{
	  private final static String TAG="AlbumImageAdapter";
	  private ArrayList<PhotoItem> photos;
	  private Context context;
	  private int albumGalleryItemBackground;
	  public AlbumImageAdapter(Context context,ArrayList<PhotoItem> photos)
	  {
	    this.context=context;
	    this.photos=photos;
	    TypedArray ta=context.obtainStyledAttributes(R.styleable.AlbumGallery);
	    albumGalleryItemBackground=ta.getResourceId(R.styleable.AlbumGallery_android_galleryItemBackground,0);
	    ta.recycle();
	  }
	  @Override
	  public int getCount()
	  {
	    return(this.photos.size());
	  }
	  @Override
	  public Object getItem(int position)
	  {
	    return(this.photos.get(position));
	  }
	  @Override
	  public long getItemId(int position){
	    return(position);
	  }
	  @Override
	  public View getView(int position,View convertView,ViewGroup parent)
	  {
	    ImageView imageView=new ImageView(context);
	    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
	    imageView.setBackgroundResource(albumGalleryItemBackground);
	    //load the image
	    String pictureUrl=this.photos.get(position).getPictureUrl();
	    try
	    {
	      Bitmap bitmap=ImageCache.get(pictureUrl);
	      if(bitmap==null)
	      {
	        bitmap=HttpFetch.fetchBitmap(pictureUrl);
	        ImageCache.put(pictureUrl,bitmap);
	      }
	      imageView.setImageBitmap(bitmap);
	    }catch(Exception x)
	    {
	      Log.e(TAG,"getView",x);
	    }
	    return(imageView);
	  }
	}