package com.example.facebookalbumdemo;

public class PhotoItem
{
	  private String pictureUrl;
	  private String sourceUrl;
	  public PhotoItem(String pictureUrl,String sourceUrl)
	  {
	    this.pictureUrl=pictureUrl;
	    this.sourceUrl=sourceUrl;
	  }
	  public String getPictureUrl()
	  {
	    return pictureUrl;
	  }
	  public void setPictureUrl(String pictureUrl)
	  {
	    //fix for Android 2.3
	    CharSequence target="\\/";
	    CharSequence replace="/";
	    String fixedUrl=pictureUrl.replace(target,replace);
	    this.pictureUrl=fixedUrl;
	  }
	  public String getSourceUrl()
	  {
	    return sourceUrl;
	  }
	  public void setSourceUrl(String sourceUrl)
	  {
	    this.sourceUrl=sourceUrl;
	  }
	}