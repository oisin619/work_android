package com.example.temperatureconverter;

public class Fahrenheit extends Temperature
{
	//Fahrenheit class to set the boiling and freezing points of water in Fahrenheit
	public Fahrenheit()
	{
		super.setGasState(212);
		super.setLiquidState(32);
		
	}
	
}
