package com.example.temperatureconverter;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity
{
	//Declare variables. Text views and edit texts must be private to avoid null pointer exception
	private EditText mEdit;
	private TextView mText;
	private TextView label;
	Celcius c = new Celcius();
	Fahrenheit f = new Fahrenheit();

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Create buttons,textfields, and declare listeners
		Button fahrenHeitButton = (Button)findViewById(R.id.fahrenheitButton);
		Button celciusButton = (Button)findViewById(R.id.celciusButton);
		mEdit = (EditText)findViewById(R.id.editText1);
		mText = (TextView)findViewById(R.id.text1);
		label = (TextView)findViewById(R.id.label);
		fahrenHeitButton.setOnClickListener(listener);
		celciusButton.setOnClickListener(listener);

		

	}
	
	View.OnClickListener listener = new View.OnClickListener()
	{
		//Click events
		@Override
		public void onClick(View v)
		{
			//Checks to see if the EditText field is empty before trying to parse its value
			double editNum = 0;
			try
			{
				if(mEdit != null)
				{
					//changes the string in the EditText field to a double
					editNum = Double.parseDouble(mEdit.getText().toString());
				}
				else
				{
					//We have a problem
				}
			}
			catch(NumberFormatException e)
			{
				//input was no number or an empty string
			}
			
			//Checks to see which of the buttons has been pressed
			switch(v.getId())
			{
			case R.id.fahrenheitButton:
				f.setTemp(editNum);
				label.setText(f.checkState());
				break;
			case R.id.celciusButton:
				c.setTemp(editNum);
				label.setText(c.checkState());
				break;
			}
			
		}
	};
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
