package com.example.temperatureconverter;

//Temperature class to store different variables and check the state of matter the water is in
public  class Temperature
{
	//Declare variables
	private double temp = 0;
	private double gasState = 0;
	private double liquidState = 0;
	
	//Constructor
	public Temperature()
	{

	}
	//Accessor and mutator for temperature(temp)
	public double getTemp()
	{
		return temp;
	}
	public void setTemp(double temp)
	{
		this.temp = temp;
	}
	//Accessor and mutator for gas state
	public double getGasState()
	{
		return gasState;
	}
	public void setGasState(double gasState)
	{
		this.gasState = gasState;
	}
	//Accessor and mutator for liquid state
	public double getLiquidState()
	{
		return liquidState;
	}
	public void setLiquidState(double liquidState)
	{
		this.liquidState = liquidState;
	}
	
	//Checks the temperature to see if the water is solid/liquid/gas
	public String checkState()
	{
		String state = "";
		if(temp >= gasState)
		{
			state = "Water is gas";
		}
		else if(temp <= liquidState)
		{
			state = "Water is solid";
		}
		else
		{
			state = "Water is liquid";
		}
		return state;
	}
	

}
