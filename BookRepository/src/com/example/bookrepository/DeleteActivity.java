package com.example.bookrepository;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DeleteActivity extends Activity implements OnClickListener 
{
	private EditText delete;
	private TextView deleteText;
	private ArrayList<Book> library = new ArrayList<Book>();

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delete);

		Button deleteButton = (Button)findViewById(R.id.deleteButton);

		delete = (EditText)findViewById(R.id.deleteId);
		deleteText = (TextView)findViewById(R.id.deleteDetails);

		deleteButton.setOnClickListener(this);

		Bundle b = getIntent().getExtras();
		library = b.getParcelableArrayList("Library");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.deleteButton:
			Bundle deleteData = new Bundle();
			int deleteId;
			if(delete!=null)
			{
				deleteId = Integer.parseInt(delete.getText().toString());
				Iterator<Book> itr = library.iterator();
				while(itr.hasNext())
				{
					Book c = itr.next();
					if(deleteId == c.getId())
					{
						itr.remove();
						Toast.makeText(getApplicationContext(), "Book deleted",Toast.LENGTH_LONG).show();
						deleteData.putParcelableArrayList("DeleteLibrary", library);
						Intent output = new Intent();
						output.putExtras(deleteData);
						setResult(RESULT_OK, output);
						finish();
						break;
					}
				}
				Toast.makeText(getApplicationContext(), "Error: Empty or invalid inputs. Try again",Toast.LENGTH_LONG).show();

			}
		}

	}



}
