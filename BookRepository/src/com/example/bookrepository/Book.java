package com.example.bookrepository;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable
{
	private String name;
	private String author;
	private int id;

	public Book(int id,String name, String author)
	{
		this.name = name;
		this.author = author;
		this.id = id;

	}

	public Book(Parcel in)
	{
		super();
		readFromParcel(in);
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author) 
	{
		this.author = author;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}



	public int describeContents() 
	{
		// TODO Auto-generated method stub
		return 0;
	}
	public static final Parcelable.Creator<Book>CREATOR = new Parcelable.Creator<Book>()
			{
		public Book createFromParcel(Parcel in)
		{
			return new Book(in);
		}

		@Override
		public Book[] newArray(int size)
		{
			return new Book[size];
		}
			};

			@Override
			public void writeToParcel(Parcel out, int flags) 
			{
				out.writeString(name);
				out.writeString(author);
				out.writeInt(id);

			}

			private void readFromParcel(Parcel in)
			{
				name = in.readString();
				author = in.readString();
				id = in.readInt();



			}



}
