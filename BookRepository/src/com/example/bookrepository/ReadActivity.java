package com.example.bookrepository;

import java.util.ArrayList;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ReadActivity  extends Activity implements OnClickListener
{
	private EditText idInput;
	private TextView results;
	private ArrayList<Book> library = new ArrayList<Book>();

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_read);

		Button readButton = (Button)findViewById(R.id.readButton);
		Button closeButton = (Button)findViewById(R.id.closeButton);

		idInput = (EditText)findViewById(R.id.readId);
		results = (TextView)findViewById(R.id.printDetails);

		readButton.setOnClickListener(this);
		closeButton.setOnClickListener(this);

		Bundle b = getIntent().getExtras();
		library = b.getParcelableArrayList("Library");

	}

	@Override
	public void onClick(View v) 
	{
		int editNum = 0;
		try{
			if(idInput != null)
			{
				editNum = Integer.parseInt(idInput.getText().toString());
			}
			else
			{
				results.setText("Invalid Input: Try again");
			}
		}
		catch(NumberFormatException e)
		{

		}

		switch(v.getId())
		{
		case R.id.readButton:
			for(Book c : library)
			{
				if(c.getId()== editNum)
				{
					results.setText("ID: "+c.getId()+"\nName: "+c.getName()+"\nAuthor: "+c.getAuthor());
					break;
				}
				else
				{
					results.setText("Book is not in library");
				}
				
			}
			
			break;
			
		case R.id.closeButton:
			finish();
			break;
		}

	}



}
