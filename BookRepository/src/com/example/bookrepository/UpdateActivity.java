package com.example.bookrepository;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateActivity extends Activity implements OnClickListener
{
	private EditText idInput;
	private EditText nameInput;
	private EditText authorInput;
	private TextView idInfo;
	private TextView updateInfo;
	private ArrayList<Book> library = new ArrayList<Book>();

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update);
		
		Button updateButton = (Button)findViewById(R.id.updateButton);
		updateButton.setOnClickListener(this);
		
		idInput = (EditText)findViewById(R.id.updateId);
		nameInput = (EditText)findViewById(R.id.updateName);
		authorInput = (EditText)findViewById(R.id.updateAuthor);
		
		idInfo = (TextView)findViewById(R.id.updateDetails);
		updateInfo = (TextView)findViewById(R.id.updateBook);
		
		Bundle b = getIntent().getExtras();
		library = b.getParcelableArrayList("Library");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.updateButton:
			int updateid;
			String updateName;
			String updateAuthor;
			Bundle updateData = new Bundle();
			
			if(idInput != null && nameInput != null && authorInput != null)
			{
				updateid = Integer.parseInt(idInput.getText().toString());
				updateName = nameInput.getText().toString();
				updateAuthor = authorInput.getText().toString();
				
				for(Book c : library)
				{
					if(updateid == c.getId())
					{
						c.setName(updateName);
						c.setAuthor(updateAuthor);
						Toast.makeText(getApplicationContext(), "Book Updated.",Toast.LENGTH_LONG).show();
						updateData.putParcelableArrayList("UpdateLibrary",library);
						Intent output = new Intent();
						output.putExtras(updateData);
						setResult(RESULT_OK,output);
						finish();
						break;
					}
				}
				Toast.makeText(getApplicationContext(), "Error: Empty or invalid inputs. Try again",Toast.LENGTH_LONG).show();
			}
		}
		
	}



}
