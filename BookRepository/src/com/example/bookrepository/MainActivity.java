package com.example.bookrepository;

import java.util.ArrayList;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener
{
	ArrayList<Book> library = new ArrayList<Book>();
	Book dexter = new Book(1,"Darkly Dreaming Dexter", "Jeff Lindsay");
	Book hungerGames = new Book(2, "The Hunger Games", "Suzanne Collins");
	Book gameOT = new Book(3,"A Game Of Thrones", "George R.R Martin");
	final int createCode = 1;
	final int deleteCode = 2;
	final int updateCode = 3;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button create = (Button)findViewById(R.id.create);
		create.setOnClickListener(this);
		
		Button read = (Button)findViewById(R.id.read);
		read.setOnClickListener(this);
		
		Button update = (Button)findViewById(R.id.update);
		update.setOnClickListener(this);
		
		Button delete = (Button)findViewById(R.id.delete);
		delete.setOnClickListener(this);
		
		library.add(dexter);
		library.add(hungerGames);
		library.add(gameOT);
		
		
		
	}

	@Override
	public void onClick(View v) 
	{
		Bundle b = new Bundle();
		b.putParcelableArrayList("Library", library);
		
		switch(v.getId())
		{
		case R.id.create:
			Intent createScreen = new Intent(this, CreateActivity.class);
			createScreen.putExtras(b);
			startActivityForResult(createScreen, createCode);
			break;
			
		case R.id.update:
			Intent updateScreen = new Intent(this, UpdateActivity.class);
			updateScreen.putExtras(b);
			startActivityForResult(updateScreen, updateCode);
			break;

		case R.id.read:
			Intent readScreen = new Intent(this, ReadActivity.class);
			readScreen.putExtras(b);
			startActivity(readScreen);
			break;
			
		case R.id.delete:
			Intent deleteScreen = new Intent(this, DeleteActivity.class);
			deleteScreen.putExtras(b);
			startActivityForResult(deleteScreen, deleteCode);
			break;
			
		}
		
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == createCode && resultCode == RESULT_OK && data != null)
		{
			library = data.getParcelableArrayListExtra("Library");
		}
		else if(requestCode == deleteCode && resultCode == RESULT_OK && data != null )
		{
			library = data.getParcelableArrayListExtra("DeleteLibrary");
		}
		else if(requestCode == updateCode && resultCode == RESULT_OK && data != null )
		{
			library = data.getParcelableArrayListExtra("UpdateLibrary");
		}
	}

	

}
