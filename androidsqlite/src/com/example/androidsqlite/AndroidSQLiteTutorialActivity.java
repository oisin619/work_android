package com.example.androidsqlite;

import java.util.List;




import android.app.Activity;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AndroidSQLiteTutorialActivity extends Activity implements OnClickListener
{

	private EditText mEdit;
	private TextView mText;
	private TextView label;
	DatabaseHandler db = new DatabaseHandler(this);
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_android_sqlite_tutorial);


		mEdit = (EditText)findViewById(R.id.editText1);
		mText = (TextView)findViewById(R.id.text1);
		label = (TextView)findViewById(R.id.label);
		Button loadDetails = (Button)findViewById(R.id.loadContact);
		loadDetails.setOnClickListener(this);


		/**
		 * CRUD Operations
		 * */
		// Inserting Contacts
		Log.d("Insert: ", "Inserting .."); 
		db.addContact(new Contact("Ravi", "9100000000"));        
		db.addContact(new Contact("Srinivas", "9199999999"));
		db.addContact(new Contact("Tommy", "9522222222"));
		db.addContact(new Contact("Karthik", "9533333333"));

		// Reading all contacts
		Log.d("Reading: ", "Reading all contacts.."); 
		List<Contact> contacts = db.getAllContacts();       

		for (Contact cn : contacts) {
			String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
			// Writing Contacts to log
			Log.d("Name: ", log);
		}
		
		
	}


	//Click events
	@Override
	public void onClick(View v)
	{
		//Checks to see if the EditText field is empty before trying to parse its value
		int editNum = 0;
		try
		{
			if(mEdit != null)
			{
				//changes the string in the EditText field to a double
				editNum = Integer.parseInt(mEdit.getText().toString());
			}
			else
			{
				//We have a problem
			}
		}
		catch(NumberFormatException e)
		{
			//input was no number or an empty string
		}
		try
		{
		Contact print = db.getContact(editNum);
		label.setText(print.print());
		}
		catch(CursorIndexOutOfBoundsException f)
		{
			label.setText("There is no contact with that ID.");
		}
		



	}

}